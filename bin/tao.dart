import 'dart:io';

class tao {
  void category() {
    print("Please select your category");
    print("1: Hot Coffee");
    print("2: Iced Coffee");
    print("3: Hot Milk");
    print("4: Iced Milk");
    print("5: Hot Tea");
    print("6: Iced Tea");
    print("7: Protein Shakes");
    print("8: Fruity Drinks");
    print("9: Soda");
  }

  void hotCof() {
    print("Please select your order");
    print("1: Espresso");
    print("2: Hot Americano");
    print("3: Hot Cafe'Latte");
    print("4: Hot Cappuccino");
    print("5: Hot Mocha");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Espresso is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Hot Americano is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Hot Cafe'Latte is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Hot Cappuccino is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Hot Mocha is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void iceCof() {
    print("Please select your order");
    print("1: Iced Espresso");
    print("2: Iced Americano");
    print("3: Iced Cafe'Latte");
    print("4: Iced Cappuccino");
    print("5: Iced Mocha");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Iced Espresso is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Iced Americano is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Iced Cafe'Latte is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Iced Cappuccino is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Iced Mocha is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void hotMilk() {
    print("Please select your order");
    print("1: Hot Caramel Milk");
    print("2: Hot Kokuto Milk");
    print("3: Hot Cocoa");
    print("4: Hot Caramel Cocoa");
    print("5: Hot Milk");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Hot Caramel Milk is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Hot Kokuto Milk is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Hot Cocoa is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Hot Caramel Cocoa is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Hot Milk is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void iceMilk() {
    print("Please select your order");
    print("1: Iced Caramel Milk");
    print("2: Iced Kokuto Milk");
    print("3: Iced Cocoa");
    print("4: Iced Caramel Cocoa");
    print("5: Iced Pink Milk");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Iced Caramel Milk is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Iced Kokuto Milk is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Iced Cocoa is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Iced Caramel Cocoa is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Iced Pink Milk is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void hotTea() {
    print("Please select your order");
    print("1: Hot Thai Milk Tea");
    print("2: Hot Taiwanese Tea");
    print("3: Hot Matcha Latte");
    print("4: Hot Black Tea");
    print("5: Hot Kokuto Tea");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Hot Thai Milk Tea is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Hot Taiwanese Tea is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Hot Matcha Latte is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Hot Black Tea is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Hot Kokuto Tea is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void iceTea() {
    print("Please select your order");
    print("1: Iced Thai Milk Tea");
    print("2: Iced Taiwanese Tea");
    print("3: Iced Matcha Latte");
    print("4: Iced Tea");
    print("5: Iced Kokuto Tea");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Iced Thai Milk Tea is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Iced Taiwanese Tea is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Iced Matcha Latte is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Iced Tea is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Iced Kokuto Tea is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void protein() {
    print("Please select your order");
    print("1: Matcha Protein Shake");
    print("2: Chocolate Protein Shake");
    print("3: Strawberry Protein Shake");
    print("4: Espresso Protein Shake");
    print("5: Thai Tea Protein Shake");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Matcha Protein Shake is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Chocolate Protein Shake is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Strawberry Protein Shake is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Espresso Protein Shake is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Thai Tea Protein Shake is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void fruit() {
    print("Please select your order");
    print("1: Iced Limeade");
    print("2: Iced Lychee");
    print("3: Iced Strawberry");
    print("4: Iced Blueberry");
    print("5: Iced Plum");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Iced Limeade is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Iced Lychee is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Iced Strawberry is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Iced Blueberry is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Iced Plum is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }

  void soda() {
    print("Please select your order");
    print("1: Iced Limeade Soda");
    print("2: Iced Lychee Soda");
    print("3: Iced Strawberry Soda");
    print("4: Iced Blueberry Soda");
    print("5: Iced Plum Soda");
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      print("Iced Limeade Soda is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 2) {
      print("Iced Lychee Soda is 50 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 3) {
      print("Iced Strawberry Soda is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 4) {
      print("Iced Blueberry Soda is 45 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    }
    if (x == 5) {
      print("Iced Plum Soda is 30 Bath Confirm?");
      print("1: Yes");
      print("2: No");
      int y = int.parse(stdin.readLineSync()!);
      if (y == 1) {
        print("Thank you for your order");
      }
      if (y == 2) {
        return;
      }
    } else {
      return;
    }
  }
}
