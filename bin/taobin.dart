import 'dart:io';
import 'tao.dart';

void main() {
  tao coffee = new tao();
  while (true) {
    coffee.category();
    int x = int.parse(stdin.readLineSync()!);
    if (x == 1) {
      coffee.hotCof();
    } else if (x == 2) {
      coffee.iceCof();
    } else if (x == 3) {
      coffee.hotMilk();
    } else if (x == 4) {
      coffee.iceMilk();
    } else if (x == 5) {
      coffee.hotTea();
    } else if (x == 6) {
      coffee.iceTea();
    } else if (x == 7) {
      coffee.protein();
    } else if (x == 8) {
      coffee.fruit();
    } else if (x == 9) {
      coffee.soda();
    } else if (x == 0) {
      exit(0);
    }
  }
}
